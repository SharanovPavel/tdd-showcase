﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace Showcase.Tests
{
    public class PermutationForestTests
    {
        [Fact]
        public void Make_forest()
        {
            var sequence = new[] {0, 1, 2, 3, 4};
            var deep = 5;

            var forest = PermutationForest.Build(deep, sequence);
            forest.Should().NotBeNull();
        }

        [Theory]
        [MemberData(nameof(Build_rows_data))]
        public void Build_rows(int[] sequence, int depth, int expectedCount)
        {
            var forest = PermutationForest.Build(depth, sequence);
            var rows = forest.BuildRows().ToList();
            rows.Should().HaveCount(expectedCount);
        }

        public static IEnumerable<object[]> Build_rows_data()
        {
            yield return new object[] {new[] {10, 11, 12, 13}, 2, 12};
            yield return new object[] {new[] {10, 11, 12, 13, 14}, 2, 20};
            yield return new object[] {new[] {10, 11, 12, 13}, 3, 24};
        }

        [Theory]
        [MemberData(nameof(Build_unique_rows_data))]
        public void Build_unique_rows(int[] sequence, int depth, int expectedCount)
        {
            var forest = PermutationForest.Build(depth, sequence);
            var rows = forest.BuildUniqueRows();
            rows.Should().HaveCount(expectedCount);
        }
        
        public static IEnumerable<object[]> Build_unique_rows_data()
        {
            yield return new object[] {new[] {10, 11, 12, 13}, 2, 12};
            yield return new object[] {new[] {10, 11, 12, 13, 14}, 2, 20};
            yield return new object[] {new[] {10, 11, 12, 13}, 3, 24};
            yield return new object[] {new[] {10, 10, 12, 13}, 2, 7};
            
        }
    }
}
