﻿using System.Collections.Generic;
using FluentAssertions;
using Xunit;

namespace Showcase.Tests
{
    public class ShowcaseToolsTests
    {
        [Fact]
        public void Count_permutations()
        {
            var rowLength = 7;
            var items = new List<(int type, int amount)>
            {
                (1, 3),
                (2, 4),
                (3, 2),
                (4, 5)
            };
            var permutationCount = ShowcaseTools.CountPermutations(items, rowLength);
            permutationCount.Should().BeGreaterThan(11039);
        }
    }
}