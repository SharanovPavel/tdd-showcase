﻿using System.Linq;
using FluentAssertions;
using Xunit;

namespace Showcase.Tests
{
    public class AggregationTests
    {
        [Fact]
        public void Sum()
        {
            var seq = new[] {1, 2, 3};
            var expected = 6;
            var actual = 0;
            foreach (var value in seq)
            {
                actual += value;
            }
            
            
            //var actual = seq.Aggregate(0, (current,value) => current + value);

            actual.Should().Be(expected);
        }
    }
}