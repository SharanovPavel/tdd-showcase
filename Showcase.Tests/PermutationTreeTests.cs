﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace Showcase.Tests
{
    public class PermutationTreeTests
    {
        // [Theory]
        // [MemberData(nameof(CountLeafData))]
        // public void Count_leaf(int deep, int[] sequence, int root, int leafCount)
        // {
        //     var tree = PermutationTree.Build(deep, sequence, root);
        //
        //     tree.CountLeaf().Should().Be(leafCount);
        // }
        //
        // public static IEnumerable<object[]> CountLeafData()
        // {
        //     yield return new object[]
        //     {
        //         3, new[] {0, 1, 2}, 0, 2
        //     };
        //
        //     yield return new object[]
        //     {
        //         3, new[] {0, 1, 2}, 1, 2
        //     };
        //     
        //     yield return new object[]
        //     {
        //         3, new[] {0, 1, 2, 3}, 0, 6
        //     };
        // }

        [Fact]
        public void Build_tree()
        {
            var tree = PermutationTree.Build(3, new[] {10, 11, 12}, 0);
            
            tree.Root.Value.Should().Be(10);
            
            tree.Root.Children.Should().HaveCount(2);
            
            foreach (var child in tree.Root.Children)
            {
                child.Children.Should().HaveCount(1, $"child '{child.Value}' should have children");

                foreach (var childChild in child.Children)
                {
                    childChild.Children.Should().BeEmpty();
                }
            }
        }

        [Theory]
        [MemberData(nameof(Get_leaves_count_data))]
        public void Get_leaves_count(int depth, int[] sequence, int rootIndex, int leavesCount)
        {
            var tree = PermutationTree.Build(depth, sequence, rootIndex);
            var leaves = tree.GetLeaves();
            leaves.Should().HaveCount(leavesCount);
        }

        public static IEnumerable<object[]> Get_leaves_count_data()
        {
            yield return new object[] {3, new[] {10, 11, 12}, 0, 2};
            yield return new object[] {2, new[] {10, 11, 12}, 0, 2};
            yield return new object[] {1, new[] {10, 11, 12}, 0, 1};
            yield return new object[] {3, new[] {10, 11, 12, 13}, 0, 6};
            yield return new object[] {4, new[] {10, 11, 12, 13}, 0, 6};
            yield return new object[] {2, new[] {10, 11, 12, 13}, 0, 3};
        }

        [Theory]
        [MemberData(nameof(Get_leaves_row_data))]
        public void Get_leaf_row(int depth, int[] sequence, int rootIndex, int[][] rows)
        {
            var tree = PermutationTree.Build(depth, sequence, rootIndex);
            var leaves = tree.GetLeaves().ToList();
            leaves.Should().HaveCount(rows.Length);

            for (var i = 0; i < rows.Length; i++)
            {
                leaves[i].BuildRow().Should().BeEquivalentTo(rows[i]);
            }
        }
        
        public static IEnumerable<object[]> Get_leaves_row_data()
        {
            yield return new object[]
            {
                2, new[] {10, 11, 12}, 0, 
                new[]
                {
                    new[] {10, 11}, 
                    new[] {10, 12}
                }
            };
            
            yield return new object[]
            {
                3, new[] {10, 11, 12, 13}, 0, 
                new[]
                {
                    new[] {10, 11, 12}, 
                    new[] {10, 11, 13},
                    new[] {10, 12, 11},
                    new[] {10, 12, 13},
                    new[] {10, 13, 11},
                    new[] {10, 13, 12}
                }
            };
        }
        
    }
}