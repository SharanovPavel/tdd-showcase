﻿using FluentAssertions;
using Xunit;

namespace Showcase.Tests
{
    public class PermutationTreeNodeTests
    {
        [Fact]
        public void Root_depth_equals_1()
        {
            var root = new PermutationTreeNode(10);
            root.Depth.Should().Be(1);
        }

        [Fact]
        public void Child_node_depth_should_be_2()
        {
            var child = new PermutationTreeNode(10,
                new PermutationTreeNode(11));

            child.Depth.Should().Be(2);
        }
    }
}