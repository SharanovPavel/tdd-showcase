﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Showcase;

namespace ShowcaseConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var rowLength = 7;
            var items = new List<(int type, int amount)>
            {
                (1, 3),
                (2, 4),
                (3, 2),
                (4, 5)
            };
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var permutations = ShowcaseTools.MakePermutations(items, rowLength).ToList();
            stopwatch.Stop();
            var elapsed = stopwatch.Elapsed;
            
            Console.WriteLine($"Elapsed: {elapsed}");

            var lines = permutations.Select(x => string.Join(" ", x));

            File.WriteAllLines("log.txt", lines);
        }
    }
}