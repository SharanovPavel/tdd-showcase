﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Showcase
{
    public class PermutationTree
    {
        public PermutationTreeNode Root { get; }
        
        private PermutationTree(PermutationTreeNode root)
        {
            Root = root;
        }

        public static PermutationTree Build(int depth, int[] sequence, int rootIndex)
        {
            var root = BuildRoot(sequence, rootIndex, depth);
            return new PermutationTree(root);
        }

        private static PermutationTreeNode BuildRoot(int[] sequence, int rootIndex, int depth)
        {
            var root = BuildNode(sequence.ToList(), rootIndex, depth);
            return root;
        }

        private static PermutationTreeNode BuildNode(List<int> values, int index, int maxDepth, PermutationTreeNode? parent = null)
        {
            var list = values.ToList();
            var value = list[index];
            var node = new PermutationTreeNode(value, parent);
            list.Remove(value);
            if (node.Depth < maxDepth)
            {
                for (var i = 0; i < list.Count; i++)
                {
                    var child = BuildNode(list, i, maxDepth, node);
                    node.AddChildren(child);
                }
            }

            return node;
        }
        
        public IEnumerable<PermutationTreeNode> GetLeaves()
        {
            static bool NodeIsLeaf(PermutationTreeNode node) => node.Children.Count == 0;

            static IEnumerable<PermutationTreeNode> Distinct(PermutationTreeNode node)
            {
                return new[] {node}.Union(node.Children.SelectMany(Distinct));
            }

            var nodes = Distinct(Root);
            return nodes.Where(NodeIsLeaf);
        }
    }
}