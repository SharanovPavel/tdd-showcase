﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Showcase
{
    public class PermutationTreeNode
    {
        public int Value { get; }
        
        public PermutationTreeNode? Parent { get; private set; }

        public ICollection<PermutationTreeNode> Children { get; set; }

        public int Depth => (Parent?.Depth ?? 0) + 1;
        
        public PermutationTreeNode(int value, PermutationTreeNode? parent = null)
        {
            Parent = parent;
            Value = value;
            Children = new List<PermutationTreeNode>();
        }

        public void AddChildren(params PermutationTreeNode[] children)
        {
            foreach (var child in children)
            {
                child.Parent = this;
                Children.Add(child);
            }
        }

        public int[] BuildRow()
        {
            var row = BuildRowInternal().ToArray();
            return row;
        }

        private List<int> BuildRowInternal()
        {
            if (Parent == null)
                return new() {Value};
            
            var row = Parent.BuildRowInternal();
            row.Add(Value);
            return row;
        }
    }
}