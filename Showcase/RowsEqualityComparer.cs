﻿using System.Collections.Generic;
using System.Linq;

namespace Showcase
{
    public class RowsEqualityComparer : IEqualityComparer<int[]>
    {
        public bool Equals(int[]? x, int[]? y)
        {
            if (x == null || y == null)
                return false;
            
            if (x.Length != y.Length)
                return false;

            for (var i = 0; i < x.Length; i++)
            {
                var xValue = x[i];
                var yValue = y[i];
                if (xValue != yValue)
                    return false;
            }

            return true;
        }

        public int GetHashCode(int[] obj)
        {
            return obj.Aggregate(1, (i, i1) => (i | i1) * 87117);
        }
    }
}