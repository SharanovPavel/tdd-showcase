﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Showcase
{
    public class PermutationForest
    {
        private readonly List<PermutationTree> _trees;
        
        private PermutationForest(IEnumerable<PermutationTree> trees)
        {
            _trees = trees.ToList();
        }

        public static PermutationForest Build(int deep, IEnumerable<int> sequence)
        {
            var list = sequence.ToArray();
            var trees = 
                list.Select((_, index) => PermutationTree.Build(deep, list, index));
            return new PermutationForest(trees);
        }

        public IEnumerable<int[]> BuildRows()
        {
            return _trees.SelectMany(tree => tree.GetLeaves().Select(node => node.BuildRow()));
        }

        public IEnumerable<int[]> BuildUniqueRows()
        {
            var rows = BuildRows();
            return rows.Distinct(new RowsEqualityComparer());
        }
    }
}