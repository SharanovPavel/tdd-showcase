﻿using System.Collections.Generic;
using System.Linq;

namespace Showcase
{
    public static class ShowcaseTools
    {
        public static int CountPermutations(List<(int type, int amount)> items, int rowLength)
        {
            return MakePermutations(items, rowLength).Count();
        }

        public static IEnumerable<int[]> MakePermutations(List<(int type, int amount)> items, int rowLength)
        {
            var sequence = MakeSequence(items);
            var forest = PermutationForest.Build(rowLength, sequence);
            return forest.BuildUniqueRows();
        }

        public static IEnumerable<int> MakeSequence(List<(int type, int amount)> items)
        {
            return items.SelectMany(item => Enumerable.Repeat(item.type, item.amount));
        }
    }
}